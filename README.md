# CacheService

## Abstract
The CacheService (CS) is a microservice intended to be used by other services to store and retrieve medium to large size arbitrary binary data (BLOBS).

Beside this classical CRUD (Create, Read, Update, Delete) functionality this service provides caching functionality. Caching means that stored data entries are automatically
removed, depending on cache constraints that can be set independently by each consumer service. Such group cache constraints are related to the maximum number of key entries,
the summed up size in bytes of all entries and the oldest access date of one key entry for one group. Whenever one of these cache constraints is exceeded, entries with the oldest access
timestamps are removed from the cache until the cache constraints are reached again.

Current consumers of this CacheService are the DocumentConverter (DC) service as well as the Middleware (MW) service via the integrated DocumentConverter client.
Other services like ImageConverter (IC) will use this service to store their results in a persistent way as well.

## Technical background and motivation to create a CacheService microservice
Many services (middleware services in almost any case) need some time to handle requests, e.g. document or image conversion often needs some seconds to produce the desired result outcome.
As such those services should perform these long-lasting operations only once in the best case, requiring some kind of persistent storage. Since storage space is a limited resource, stored
entries need to be removed according to some given constraints, starting with those entries that haven't been accessed for some period of time. In general this is called persistent caching
where entries are still available when a service gets restarted.

Since such services do caching in more or less the same way, there was a demand to move that caching functionality into a dedicated service providing a stateless [HTTP API](https://documentation.open-xchange.com/components/cacheservice/8/api/)
to cache such binary
data results. So the CacheService has been introduced to manage this.

The CacheService itself is a standalone service based on the industrial grade SpringBoot library.

## Implementation details
### Dependencies
In order to store BLOBs the CacheService needs to rely on some kind of object storage. Since most common object storage implementations provide an S3 API, the CacheService allows to access S3
compatible object storages. In addition, Scality/SproxyD compatible object storages are supported as well. The kind of object storage to use and necessary access data need to be configured by
the admin via appropriate values. More object storage adapters can be added as well, if required.

To keep dependencies to object storage APIs as small as possible, the metadata information (e.g. last access timestamp, size) for each stored object is kept within
a CacheService database schema. For each service (group) that registers itself with appropriate cache constraints a set of group tables is created to manage the cache handling.

When a service needs to write or read a cache entry the entry itself is addressed via a triple value consisting of:

- groupId (e.g. 'OX_DC')
- keyId (e.g. the hash value for a source document)
- fileId (the name of the file the result content is stored at)

For one groupId multiple distinct keyIds can be stored, for one distinct keyId multiple fileIds can be stored and retrieved.

The CacheService configuration of the used object storage as well as the used database is done via these
[Helm chart](https://gitlab.open-xchange.com/documents/cacheservice/-/tree/main/helm/core-cacheservice) values.

Since each registered group (consuming service) may have different requirements regarding the group specific cache constraints, a service registers itself at the CacheService during startup,
providing its own groupId as well as its own cache constraints. After this registration has been done, the groupId is ready to be used by consuming services.

## Example workflow
The diagrams on this [DocumentConverter site](https://gitlab.open-xchange.com/documents/documentconverter/-/blob/main/README.md) show a possible workflow with DocumentConverter service using the CacheService to store result data.

Configuring the DocumentConverter cache constraints to be used at CacheService registration step is done via the DocumentConverter cache related [Helm chart](https://gitlab.open-xchange.com/documents/documentconverter/-/tree/main/helm/core-documentconverter) values.
