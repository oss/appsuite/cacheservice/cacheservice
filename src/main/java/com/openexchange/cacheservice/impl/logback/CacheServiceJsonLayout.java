/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.cacheservice.impl.logback;

import static ch.qos.logback.classic.Level.DEBUG_INT;
import static ch.qos.logback.classic.Level.ERROR_INT;
import static ch.qos.logback.classic.Level.INFO_INT;
import static ch.qos.logback.classic.Level.TRACE_INT;
import static ch.qos.logback.classic.Level.WARN_INT;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.classic.JsonLayout;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Marker;

/**
 * {@link CacheServiceJsonLayout} - Defines a basic JSON layout for {@link ILoggingEvent} to be logged
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v2.1.0
 */
public class CacheServiceJsonLayout extends JsonLayout {

    private static final String DROP_MDC_MARKER = "DropMDC";

    private static final int LEVEL_ERROR = 3;
    private static final int LEVEL_WARNING = 4;
    private static final int LEVEL_INFO = 6;
    private static final int LEVEL_DEBUG = 7;
    private static final int LEVEL_TRACE = 8;

    /** The attribute name for human-readable time stamp */
    protected static final String ATTR_NAME_TIMESTAMP_READABLE = "timestampReadable";

    /** The attribute name for the name of the log level */
    protected static final String ATTR_NAME_LEVEL_NAME = "levelName";

    /** The attribute name for the name of the (Java) file in which log event has been issued */
    protected static final String ATTR_NAME_FILE ="file";

    /** The attribute name for the log message */
    protected static final String ATTR_NAME_MSG = "msg";

    /** The attribute name for the stack trace */
    protected static final String ATTR_NAME_STACKTRACE = "stacktrace";

    /** The attribute name for the line number in associated (Java) file in which log event has been issued */
    protected static final String ATTR_NAME_LINE = "line";

    /** The attribute name for the name of the method */
    protected static final String ATTR_NAME_METHOD = "method";

    /** The attribute name for details (aka MDC property map) (if present an instance of <code>Map&lt;String, String&gt;</code>) */
    protected static final String ATTR_NAME_DETAILS = "details";

    /** The flag whether to include human-readable time stamp */
    protected boolean includeReadableTimestamp;

    /** The flag whether to include the name of the log level */
    protected boolean includeLogLevelName;

    /**
     * Initializes a new {@link CacheServiceJsonLayout}.
     */
    public CacheServiceJsonLayout() {
        super();
        includeReadableTimestamp = false;
        includeLogLevelName = false;
    }

    @Override
    protected Map<String, Object> toJsonMap(ILoggingEvent event) {
        // Build a map from logging event
        Map<String, Object> map = new LinkedHashMap<>();
        add(TIMESTAMP_ATTR_NAME, this.includeTimestamp, event.getTimeStamp(), map);
        addTimestamp(ATTR_NAME_TIMESTAMP_READABLE, this.includeReadableTimestamp, event.getTimeStamp(), map);
        add(LEVEL_ATTR_NAME, this.includeLevel, getNumericLogLevel(event.getLevel()), map);
        add(ATTR_NAME_LEVEL_NAME, this.includeLogLevelName, String.valueOf(event.getLevel()), map);
        add(ATTR_NAME_FILE, this.includeLoggerName, event.getLoggerName(), map);
        if (this.includeLoggerName) {
            addCallerData(event.getCallerData(), map);
        }
        add(ATTR_NAME_MSG, this.includeFormattedMessage, event.getFormattedMessage(), map);
        add(MESSAGE_ATTR_NAME, this.includeMessage, event.getMessage(), map);
        addThrowableInfo(ATTR_NAME_STACKTRACE, this.includeException, event, map);

        Map<String, String> mdc = event.getMDCPropertyMap();
        if (!dropMdc(event, mdc)) {
            // Ensure a modifiable map
            HashMap<String, String> eventMdc = new HashMap<>(mdc);

            // Add thread name to details (aka MDC) map
            addPropertyToDetails(THREAD_ATTR_NAME, event.getThreadName(), eventMdc);

            // Add details to superior map
            addMap(ATTR_NAME_DETAILS, true, eventMdc, map);
        }

        return map;
    }

    /**
     * Adds the line and method informations from the given stack trace.
     *
     * @param stackTrace The stack trace containing the method and line information
     * @param map The map to add the information to
     */
    private void addCallerData(StackTraceElement[] stackTrace, Map<String, Object> map) {
        if (stackTrace == null || stackTrace.length == 0) {
            return;
        }
        StackTraceElement first = stackTrace[0];
        add(ATTR_NAME_LINE, true, first.getLineNumber(), map);
        add(ATTR_NAME_METHOD, true, first.getMethodName(), map);
    }

    /**
     * Checks whether specified logging event (and accompanying MDC) signals that MDC properties are supposed to be suppressed.
     *
     * @param event The logging event
     * @param mdc The MDC property map
     * @return <code>true</code> to suppress MDC properties; otherwise <code>false</code>
     */
    protected static boolean dropMdc(ILoggingEvent event, Map<String, String> mdc) {
        if ("true".equals(getPropertyFromMDC("com.openexchange.logback.dropMDC", mdc))) {
            return true;
        }

        Marker marker = event.getMarker();
        return marker != null && marker.contains(DROP_MDC_MARKER);
    }

    /**
     * Gets the denoted property from given MDC properties.
     *
     * @param propertyName The name of the property to return (if available)
     * @param mdc The MDC property map to get from
     * @return The MDC property or <code>null</code>
     */
    protected static String getPropertyFromMDC(String propertyName, Map<String, String> mdc) {
        return null == propertyName || null == mdc ? null : mdc.get(propertyName);
    }

    /**
     * Gets the line number from stack trace
     *
     * @param stackTrace The stack trace
     * @return The line number or <code>-1</code> if the stack trace is null or empty
     */
    protected static int getLineNumber(StackTraceElement[] stackTrace) {
        if (null == stackTrace || 0 == stackTrace.length) {
            return -1;
        }
        return stackTrace[0].getLineNumber();
    }

    /**
     * Adds specified property to given details (aka MDC) map.
     *
     * @param fieldName The name of the property
     * @param value The value of the property
     * @param detailsMap The details (aka MDC) map to add to
     */
    protected static void addPropertyToDetails(String fieldName, String value, Map<String, String> detailsMap) {
        if (null != fieldName && null != value && null != detailsMap) {
            detailsMap.put(fieldName, value);
        }
    }

    /**
     * Adds a <code>long</code> value to the map.
     *
     * @param fieldName The field name
     * @param field <code>true</code> if this field should be added to the map, <code>false</code> otherwise
     * @param value The <code>long</code> value
     * @param map The map
     */
    protected static void add(String fieldName, boolean field, long value, Map<String, Object> map) {
        if (field) {
            map.put(fieldName, Long.valueOf(value));
        }
    }

    /**
     * Adds an <code>int</code> value to the map.
     *
     * @param fieldName The field name
     * @param field <code>true</code> if this field should be added to the map, <code>false</code> otherwise
     * @param value The <code>int</code> value
     * @param map The map
     */
    protected static void add(String fieldName, boolean field, int value, Map<String, Object> map) {
        if (field) {
            map.put(fieldName, Integer.valueOf(value));
        }
    }

    /**
     * Adds a <code>double</code> value to the map-
     *
     * @param fieldName The field name
     * @param field <code>true</code> if this field should be added to the map, <code>false</code> otherwise
     * @param value The <code>double</code> value
     * @param map The map
     */
    protected static void add(String fieldName, boolean field, double value, Map<String, Object> map) {
        if (field) {
            map.put(fieldName, Double.valueOf(value));
        }
    }

    /**
     * Adds a <code>boolean</code> value to the map.
     *
     * @param fieldName The field name
     * @param field <code>true</code> if this field should be added to the map, <code>false</code> otherwise
     * @param value The <code>boolean</code> value
     * @param map The map
     */
    protected static void add(String fieldName, boolean field, boolean value, Map<String, Object> map) {
        if (field) {
            map.put(fieldName, Boolean.valueOf(value));
        }
    }

    /**
     * Sets whether to include a human-readable time stamp in the log entry.
     *
     * @param includeReadableTimestamp <code>true</code> to include a human-readable time stamp in the log entry, <code>false</code> otherwise
     */
    public void setIncludeReadableTimestamp(boolean includeReadableTimestamp) {
        this.includeReadableTimestamp = includeReadableTimestamp;
    }

    /**
     * Sets whether to include the log-level's name in the log entry.
     *
     * @param includeLogLevelName <code>true</code> to include the log-level's name in the log entry, <code>false</code> otherwise
     */
    public void setIncludeLogLevelName(boolean includeLogLevelName) {
        this.includeLogLevelName = includeLogLevelName;
    }

    /**
     * Maps a {@link Level} to an <code>int</code> value representing the log level in the log entry.
     * <p>
     * Possible values are:
     * <table>
     * <tr><td><code>ERROR</code><td>--&gt;<td><code>3</code>
     * <tr><td><code>WARN</code><td>--&gt;<td><code>4</code>
     * <tr><td><code>INFO</code><td>--&gt;<td><code>6</code>
     * <tr><td><code>DEBUG</code><td>--&gt;<td><code>7</code>
     * <tr><td><code>TRACE</code><td>--&gt;<td><code>8</code>
     * </table>
     *
     * @param level The log level
     * @return The <code>int</code> value representing the log level
     */
    protected static int getNumericLogLevel(Level level) {
        return switch (level.levelInt) {
            case ERROR_INT -> LEVEL_ERROR;
            case WARN_INT -> LEVEL_WARNING;
            case INFO_INT -> LEVEL_INFO;
            case DEBUG_INT -> LEVEL_DEBUG;
            case TRACE_INT -> LEVEL_TRACE;
            default -> LEVEL_WARNING;
        };
    }
}
