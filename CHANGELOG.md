# App Suite CacheService

All notable changes to this project will be documented in this file.


## [Unreleased]

## [8.34] - 2025-01-16

<!-- References -->
[8.34]: https://gitlab.open-xchange.com/documents/cacheservice/-/compare/8.33.0...8.34.0
